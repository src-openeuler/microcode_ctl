%global debug_package %{nil}

Name:           microcode_ctl
Summary:        Microcode update for CPU
Version:        20250211
Release:        1
License:        GPL-2.0-or-later and intel-mcu-2018
URL:            https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files
ExclusiveArch:  %{ix86} x86_64
Source0:        https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/archive/tags/microcode-%{version}.tar.gz

%description
This is a tool to transform and deploy microcode update for x86 CPUs.

%prep
%autosetup -n Intel-Linux-Processor-Microcode-Data-Files-microcode-%{version}
rm -rf intel-ucode intel-ucode-with-caveats

%build
tar xf %{SOURCE0} --strip 1 */intel-ucode/* */intel-ucode-with-caveats/* \
--one-top-level=intel-ucode --strip-components=2 --backup=simple

%install
install -d %{buildroot}/lib/firmware/intel-ucode
install -m 644 intel-ucode/* %{buildroot}/lib/firmware/intel-ucode
rm -rf intel-ucode

%files
/lib/firmware/*

%changelog
* Wed Feb 12 2025 Funda Wang <fundawang@yeah.net> - 20250211-1
- update to 2025021 for fix CVE-2024-28047, CVE-2024-31157, CVE-2024-39279,
  CVE-2024-28127, CVE-2024-29214, CVE-2024-24582, CVE-2023-34440, CVE-2024-37020,
  CVE-2024-39355, CVE-2023-43758, CVE-2024-36293, CVE-2024-31068

* Thu Nov 14 2024 Funda Wang <fundawang@yeah.net> - 20241112-1
- update to 20241112 for fix CVE-2024-23918, CVE-2024-21853, CVE-2024-21820

* Wed Oct 30 2024 Funda Wang <fundawang@yeah.net> - 20241029-1
- update to 20241029

* Wed Sep 11 2024 Funda Wang <fundawang@yeah.net> - 20240910-1
- update to 20240910 for fix CVE-2024-24968, CVE-2024-23984

* Wed Sep 04 2024 Funda Wang <fundawang@yeah.net> - 20240813-1
- update to 20240813 for fix CVE-2024-24853, CVE-2024-25939,
  CVE-2024-24980, CVE-2023-42667, CVE-2023-49141

* Tue Aug 20 2024 xu_ping <707078654@qq.com> - 20240531-2
- License compliance rectification.

* Tue Jun 11 2024 wangkai <13474090681@163.com> - 20240531-1
- Update to 20240531 for fix CVE-2023-45733, CVE-2023-45745,
  CVE-2023-46103, CVE-2023-47855

* Thu Mar 14 2024 wangkai <13474090681@163.com> - 20240312-1
- Update to 20240312 for fix CVE-2023-38575, CVE-2023-39368,
  CVE-2023-22655, CVE-2023-43490, CVE-2023-28746

* Mon Mar 4 2024 peng.zou <peng.zou@shingroup.cn> - 20231114-2
- add ppc64le support

* Tue Jan 9 2024 liyanan <liyanan61@h-partners.com> - 20231114-1
- Modify Source0 to get it form github

* Wed Nov 15 2023 lwg K <weigangli99@gmail.com> - 2.1-42
- update to upstream 2.1-42. 20231114
- fix CVE-2023-23583

* Wed Aug 23 2023 yaoxin <yao_xin001@hoperun.com> - 2.1-41
- Update to 2.1-41 for fix CVE-2022-40982,CVE-2022-38090 and CVE-2022-33196

* Mon Aug 21 2023 liyanan <thistleslyn@163.com> - 2.1-39
- Fix compilation failure caused by tar upgrade

* Mon Nov 14 2022 zhaozhen <zhaozhen@loongson.cn> - 2.1-38
- add loongarch support

* Fri Sep 23 2022 luopihui <luopihui@Ncti-gba.cn> - 2.1-37
- Upgrade to 2.1-37
- Fix CVE-2022-21233

* Tue Jul 19 2022 houyingchao <houyingchao@h-partners.com> - 2.1-36
- Upgrade to 2.1-36
- Fix CVE-2021-0146

* Thu Sep 23 2021 yaoxin <yaoxin30@huawei.com> - 2.1-33
- update version to 2.1-33, fix CVE-2020-24511 CVE-2020-24512

* Mon Dec 14 2020 zhanghua <zhanghua40@huawei.com> - 2.1-28
- update version to 2.1-31, fix CVE-2020-8695, CVE-2020-8696, CVE-2020-8698

* Thu Nov 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.1-27
- Package init
